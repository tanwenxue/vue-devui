export const CONTRIBUTORS_MAP = {
  // 通用
  button: [
    {
      avatar: 'https://avatars.githubusercontent.com/u/13329558?v=4',
      homepage: 'https://github.com/Zcating'
    },
  ],
  dragdrop: [
    {
      avatar: '',
      homepage: 'https://github.com/asdlml6'
    },
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/2582/7746958_kagol_1606652460.png!avatar100',
      homepage: 'https://github.com/kagol'
    },
  ],
  fullscreen: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/640/1920569_micd_1633943788.png!avatar100',
      homepage: 'https://gitee.com/micd'
    },
  ],
  icon: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/2582/7746958_kagol_1606652460.png!avatar100',
      homepage: 'https://github.com/kagol',
    }
  ],
  overlay: [
    {
      avatar: '',
      homepage: 'https://github.com/Zcating'
    },
    {
      avatar: '',
      homepage: 'https://github.com/liuxdi'
    },
    {
      avatar: '',
      homepage: 'https://github.com/to0simple'
    },
  ],
  panel: [
    {
      avatar: '',
      homepage: 'https://github.com/GaoNeng-wWw'
    },
  ],
  ripple: [
    {
      avatar: '',
      homepage: 'https://github.com/ErKeLost'
    },
  ],
  search: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/615/1847969_laiweilun_1628090818.png!avatar100',
      homepage: 'https://github.com/SituC'
    },
  ],
  status: [
    {
      avatar: '',
      homepage: 'https://gitee.com/LiuSuY'
    },
  ],
  sticky: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/2451/7354663_maizhiyuan_1647402812.png!avatar200',
      homepage: 'https://gitee.com/maizhiyuan'
    },
  ],

  // 导航
  accordion: [
    {
      avatar: '',
      homepage: 'https://github.com/Pineapple0919'
    },
    {
      avatar: '',
      homepage: 'https://github.com/liuxdi'
    },
  ],
  anchor: [
    {
      avatar: '',
      homepage: 'https://gitee.com/asian-TMac'
    },
  ],
  'back-top': [
    {
      avatar: '',
      homepage: 'https://github.com/c0dedance'
    },
  ],
  breadcrumb: [
    {
      avatar: '',
      homepage: 'https://github.com/naluduo233'
    },
  ],
  dropdown: [
    {
      avatar: '',
      homepage: 'https://github.com/Zcating'
    },
  ],
  'nav-sprite': [
    {
      avatar: '',
      homepage: 'https://gitee.com/flxy1028'
    },
  ],
  pagination: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/481/1444460_kd554246839_1647266753.png!avatar200',
      homepage: 'https://gitee.com/kd554246839'
    },
  ],
  'steps-guide': [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/765/2295523_pupulus_1647268902.png!avatar200',
      homepage: 'https://gitee.com/pupulus'
    },
  ],
  tabs: [
    {
      avatar: '',
      homepage: 'https://gitee.com/flxy1028'
    },
  ],

  // 反馈
  alert: [
    {
      avatar: '',
      homepage: 'https://github.com/chressYu'
    },
    {
      avatar: '',
      homepage: 'https://github.com/liuxdi'
    },
  ],
  drawer: [
    {
      avatar: '',
      homepage: 'https://github.com/lnzhangsong'
    },
  ],
  loading: [
    {
      avatar: '',
      homepage: 'https://gitee.com/kd554246839'
    },
  ],
  modal: [
    {
      avatar: '',
      homepage: 'https://github.com/Zcating'
    },
  ],
  notification: [
    {
      avatar: '',
      homepage: 'https://github.com/liuxdi'
    },
    {
      avatar: '',
      homepage: 'https://github.com/to0simple'
    },
  ],
  popover: [
    {
      avatar: '',
      homepage: 'https://github.com/CatsAndMice'
    },
  ],
  'read-tip': [
    {
      avatar: '',
      homepage: 'https://github.com/whylost'
    },
  ],
  result: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/727/2183498_icjs-cc_1647402518.png!avatar200',
      homepage: 'https://gitee.com/icjs-cc'
    },
  ],
  tooltip: [
    {
      avatar: '',
      homepage: 'https://github.com/zxlfly'
    },
  ],

  // 数据录入
  'auto-complete': [
    {
      avatar: '',
      homepage: 'https://github.com/to0simple'
    },
    {
      avatar: '',
      homepage: 'https://github.com/asdlml6'
    },
  ],
  cascader: [
    {
      avatar: '',
      homepage: 'https://github.com/SituC'
    },
  ],
  checkbox: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/1769/5309699_brenner8023_1633954462.png!avatar200',
      homepage: 'https://gitee.com/brenner8023'
    },
    {
      avatar: '',
      homepage: 'https://gitee.com/liao-jianguo'
    },
  ],
  'color-picker': [
    {
      avatar: '',
      homepage: 'https://github.com/ErKeLost'
    },
  ],
  'date-picker': [
    {
      avatar: '',
      homepage: 'https://gitee.com/mrundef'
    },
  ],
  'date-picker-pro': [
    {
      avatar: '',
      homepage: 'https://github.com/naluduo233'
    },
    {
      avatar: '',
      homepage: 'https://github.com/jCodeLife'
    },
  ],
  'editable-select': [
    {
      avatar: '',
      homepage: 'https://github.com/chenzi24'
    },
  ],
  form: [
    {
      avatar: '',
      homepage: 'https://github.com/AlanLee97'
    },
  ],
  input: [
    {
      avatar: '',
      homepage: 'https://github.com/SituC'
    },
  ],
  'input-icon': [
    {
      avatar: '',
      homepage: 'https://gitee.com/mrundef'
    },
  ],
  'input-number': [
    {
      avatar: '',
      homepage: 'https://github.com/git-Where'
    },
  ],
  radio: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/2722/8168765_xuehongjie_1647269846.png!avatar100',
      homepage: 'https://github.com/xuehongjie'
    }
  ],
  select: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/3181/9545863_lookforwhat_1628521673.png!avatar200',
      homepage: 'https://gitee.com/lookforwhat'
    },
  ],
  slider: [
    {
      avatar: '',
      homepage: 'https://github.com/xiaoboRao'
    },
  ],
  switch: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/1769/5309699_brenner8023_1633954462.png!avatar200',
      homepage: 'https://gitee.com/brenner8023'
    },
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/1826/5479448_vergil_lu_1626167392.png!avatar200',
      homepage: 'https://gitee.com/vergil_lu'
    },
  ],
  'tag-input': [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/1769/5309699_brenner8023_1633954462.png!avatar200',
      homepage: 'https://gitee.com/brenner8023'
    },
    {
      avatar: '',
      homepage: 'https://gitee.com/gu_yan'
    },
  ],
  textarea: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/1747/5243550_afterain_1635492436.png!avatar200',
      homepage: 'https://gitee.com/afterain'
    },
  ],
  'time-picker': [
    {
      avatar: '',
      homepage: 'https://github.com/qq154239735'
    },
  ],
  transfer: [
    {
      avatar: '',
      homepage: 'https://github.com/ForeseeBear'
    },
  ],
  'tree-select': [
    {
      avatar: '',
      homepage: 'https://github.com/254311563'
    },
  ],
  upload: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/610/1832717_zzzautumn_1628044491.png!avatar200',
      homepage: 'https://gitee.com/zzzautumn'
    },
  ],

  // 数据展示
  avatar: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/610/1832717_zzzautumn_1628044491.png!avatar200',
      homepage: 'https://gitee.com/zzzautumn'
    },
  ],
  badge: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/640/1920569_micd_1633943788.png!avatar200',
      homepage: 'https://gitee.com/micd'
    },
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/413/1241369_sise209_1628127437.png!avatar200',
      homepage: 'https://gitee.com/sise209'
    },
  ],
  card: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/1826/5479448_vergil_lu_1626167392.png!avatar200',
      homepage: 'https://gitee.com/vergil_lu'
    },
  ],
  carousel: [
    {
      avatar: '',
      homepage: 'https://github.com/Roading'
    },
  ],
  comment: [
    {
      avatar: '',
      homepage: 'https://github.com/nextniko'
    },
  ],
  countdown: [
    {
      avatar: '',
      homepage: 'https://gitee.com/HeQinQins'
    },
  ],
  gantt: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/552/1658836_liuyingbin_1636725444.png!avatar200',
      homepage: 'https://gitee.com/liuyingbin'
    },
  ],
  'image-preview': [
    {
      avatar: '',
      homepage: 'https://github.com/liuxdi'
    },
  ],
  list: [
    {
      avatar: '',
      homepage: 'https://github.com/xingyan95'
    },
  ],
  progress: [
    {
      avatar: '',
      homepage: 'https://github.com/devin974'
    },
  ],
  'quadrant-diagram': [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/3181/9544625_nowisfuture_1647414135.png!avatar200',
      homepage: 'https://gitee.com/nowisfuture'
    },
  ],
  rate: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/610/1832717_zzzautumn_1628044491.png!avatar200',
      homepage: 'https://gitee.com/zzzautumn'
    },
  ],
  skeleton: [
    {
      avatar: '',
      homepage: 'https://github.com/ivestszheng'
    },
  ],
  statistic: [
    {
      avatar: '',
      homepage: 'https://github.com/ErKeLost'
    },
  ],
  table: [
    {
      avatar: '',
      homepage: 'https://github.com/Zcating'
    },
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/2582/7746958_kagol_1606652460.png!avatar100',
      homepage: 'https://github.com/kagol'
    },
    {
      avatar: '',
      homepage: 'https://github.com/ivestszheng'
    },
    {
      avatar: '',
      homepage: 'https://gitee.com/georgeleeo_jxd'
    },
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/757/2273663_weban_1611113629.png!avatar200',
      homepage: 'https://gitee.com/weban'
    },
  ],
  tag: [
    {
      avatar: '',
      homepage: 'https://github.com/c0dedance'
    },
  ],
  timeline: [
    {
      avatar: '',
      homepage: 'https://gitee.com/jenson-miao'
    },
  ],
  tree: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/2582/7746958_kagol_1606652460.png!avatar100',
      homepage: 'https://github.com/kagol'
    },
    {
      avatar: '',
      homepage: 'https://github.com/faq0192'
    },
    {
      avatar: '',
      homepage: 'https://github.com/linxiang07'
    },
  ],

  // 布局
  grid: [
    {
      avatar: '',
      homepage: 'https://github.com/ming-bin'
    },
  ],
  layout: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/601/1804112_tanwenxue_1647402605.png!avatar200',
      homepage: 'https://gitee.com/tanwenxue'
    },
  ],
  splitter: [
    {
      avatar: 'https://portrait.gitee.com/uploads/avatars/user/2528/7584294_Jecyu_1630023376.png!avatar100',
      homepage: 'https://github.com/naluduo233'
    },
  ],

  // 暂无田主
  menu: [],
  dashboard: [],
}